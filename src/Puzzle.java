//Sources: 
//https://docs.oracle.com/javase/7/docs/api/java/util/Hashtable.html
//https://docs.oracle.com/javase/7/docs/api/java/util/HashMap.html
//http://www.kosbie.net/cmu/fall-09/15-110/handouts/recursion/Cryptarithms.java
//https://www.youtube.com/watch?v=HC6Y49iTg1k
//https://www.youtube.com/watch?v=-UGzrsVFneo
//http://www.greylabyrinth.com/solution/puzzle001
//http://ken.duisenberg.com/potw/archive/arch97/970710sol.html
//http://www.freemathhelp.com/forum/threads/46077-letter-puzzle-SEND-MORE-MONEY

import java.util.HashMap;
import java.util.HashSet;

public class Puzzle 
{
	private static int permutations;
	private static String letters;
	private static String[]words;
	
	public static void main(String[] args) 
	{
		cryptarithm("SEND","MORE","MONEY");
		//cryptarithm(args[0],args[1],args[2]);
	}//main
	
	public static void cryptarithm(String string1,String string2,String string3)
	{
		words = new String[3];
		words[0] = string1;
		words[1] = string2;
		words[2] = string3;
		String allWords = string1 + string2 + string3;
		letters = "";
		
		for (int index = 0; index < allWords.length(); index++) {
			char character = allWords.charAt(index);
			if(letters.indexOf(character) < 0)
		    letters += character;
		}
		permute(10,letters.length());
	}//cryptarithm
	
	public static void permute(int n, int k)
	{
		permute(n,k, new HashSet<Integer>(), new int[k]);
	}//permute
	
	public static void permute(int n, int k, HashSet<Integer> set, int[]permutation)
	{
		if(set.size() == k)
			doPermute(n, k, set, permutation);
		else
		{
			for (int index = 0; index < n; index++) 
			{
				if(!set.contains(index))
				{
					permutation[set.size()] = index;
					set.add(index);
					permute(n,k,set,permutation);
					set.remove(index);
				}
			}
		}
	}//permute
	
	public static void doPermute(int n, int k, HashSet<Integer> set, int[]permutation)
	{
		HashMap<Character,Integer> characterMap = new HashMap<Character, Integer>();
		for (int index = 0; index < k; index++) 
			characterMap.put(letters.charAt(index), //"SENDMORY"
					         permutation[index]);   //[2,3,1,4,0,5]
		int[]vals = new int[3];
		for (int index2 = 0; index2 < vals.length; index2++) 
		{
			String word = words[index2];
			if(characterMap.get(word.charAt(0)) == 0)
				return;
			int val = 0;
			for (int index = 0; index < word.length(); index++)
				val = 10*val + characterMap.get(word.charAt(index));
			vals[index2] = val;
		}
		if(vals[0] + vals[1] == vals[2])
		{
			System.out.println(characterMap);
			System.out.format("%8d\n+%7d\n=%7d\n",vals[0],vals[1],vals[2]);
		}
	}//doPermute
	
}//class Puzzle